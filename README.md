# Personal ReactJs Site 

Simple Site (actually not) made with ReactJs and friends, the most efficient website i've ever created.

## **the site is deployed** in https://mikailthoriq.my.id

# illustrations 🍥

- [UnDraw](https://undraw.co/illustrations)

# Technologies used 🛠️
<p align="left"> <a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://graphql.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/graphql/graphql-icon.svg" alt="graphql" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://reactjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> <a href="https://redux.js.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/redux/redux-original.svg" alt="redux" width="40" height="40"/> </a> </p>


![App Screenshot](https://server3.riqq-cdn.cf/0:/Pics/we-learn-javascript-not-because-it-is-easy-but-because-it-is-hard.jpg)

# References

Based on [ashutosh1919 / masterPortfolio](https://github.com/ashutosh1919/masterPortfolio/) and [saadpasta / developerFolio](https://github.com/saadpasta/developerFolio)